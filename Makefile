DIR := ${CURDIR}

-include $(DIR)/config.mk


MARIADB_VERSION := $(or $(MARIADB_VERSION),latest)

DB_PORT := $(or $(DB_PORT),3306)
DB_ROOT_PW := $(or $(DB_ROOT_PW),secret)

DI_NAME := $(or $(DI_NAME),graphi/mariadb)
DI_TAG := $(or $(DI_TAG),0.1.1)
DC_NAME := $(or $(DC_NAME),mariadb)

DI_ID := $(shell docker images | grep "$(DI_NAME)\s*$(DI_TAG)" | sed -n "s/[0-9a-z\/-]*\s*[0-9\.]*\s*\([0-9a-z]*\).*/\1/p")
DC_ID := $(shell docker ps -a | grep "$(DI_NAME):$(DI_TAG)" | sed -n "s/\([0-9a-z]*\).*/\1/p")
DC_STARTED_ID := $(shell docker ps | grep "$(DI_NAME):$(DI_TAG)" | sed -n "s/\([0-9a-z]*\).*/\1/p")


docker-bash: docker-start
		$(eval DC_ID := $(shell docker ps -a | grep "$(DI_NAME):$(DI_TAG)" | sed -n "s/\([0-9a-z]*\).*/\1/p"))
		docker exec -it $(DC_ID) /bin/bash

docker-build:
ifeq ($(DI_ID),)
		docker build \
			--build-arg MARIADB_VERSION=$(MARIADB_VERSION) \
			-t $(DI_NAME):$(DI_TAG) \
			./Docker
endif

docker-run: docker-build
ifeq ($(DC_ID),)
		docker run \
			-p $(DB_PORT):3306 \
			--name $(DC_NAME) \
			-e MYSQL_ROOT_PASSWORD=$(DB_ROOT_PW) \
			-d $(DI_NAME):$(DI_TAG)
endif

docker-start: docker-run
ifeq ($(DC_STARTED_ID),)
		docker start $(DC_NAME)
endif

docker-stop:
		docker stop $(DC_NAME)


docker-clean: docker-clean-container
		docker rmi $(DI_NAME):$(DI_TAG)

docker-clean-container:
		-docker stop $(DC_NAME)
		-docker rm $(DC_NAME)
